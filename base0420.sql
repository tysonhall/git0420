/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost
 Source Database       : chaochao

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : utf-8

 Date: 04/20/2019 21:57:44 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `td_api_log`
-- ----------------------------
DROP TABLE IF EXISTS `td_api_log`;
CREATE TABLE `td_api_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_api_log`
-- ----------------------------
BEGIN;
INSERT INTO `td_api_log` VALUES ('1', 'RUS2UxldfDs7VsURmRAF9xVL0BTptl069pas3cSep+wzV5Xp8zh7cEP34eAjNMjDHPHfK2MJMpQmC/LhJzizzrQ+gTv09bOP+GHnqTZ17L6YO+KM4htRnNyTJweIak8CF0sLV21a8q2rqclk9WygJbHkWnLx6fFC/k24L2PXLR5LeP8dGrGICT32zkF4DdA53aHlEGEIvhXvqOWC06mBBdv2KuNqhG+iZTAK3hA5m3WGdzZL2VoBaVOjKhWwrDyROx9F8bPwEql1TYSIxkiX8WJZpZ2qy86aVUy/+VosX9PmFs9QtbQFQRXjFCCBgeL3NuJiYikPVnyBdjjbECKVRFcldQH3ZtJzzA7FKVyJFaGvuQgoRZAMBZkeH6S8moYc0q4BvoqjhjRe6pmKN/bXQzqbglyvUO4L0oHB7rXPrL0RHs6ENtmZkUxcvTzTsAttru71YQ9R1u2a28beC/FiI48EBAAZipN6zuowWwbWpf4=', '1553402810'), ('2', 'RUS2UxldfDs7VsURmRAF9xVL0BTptl069pas3cSep+wzV5Xp8zh7cEP34eAjNMjDHPHfK2MJMpQmC/LhJzizzrQ+gTv09bOP+GHnqTZ17L6YO+KM4htRnNyTJweIak8CF0sLV21a8q2rqclk9WygJbHkWnLx6fFC/k24L2PXLR5LeP8dGrGICT32zkF4DdA53aHlEGEIvhXvqOWC06mBBdv2KuNqhG+iZTAK3hA5m3WGdzZL2VoBaVOjKhWwrDyROx9F8bPwEql1TYSIxkiX8WJZpZ2qy86aVUy/+VosX9PmFs9QtbQFQRXjFCCBgeL3NuJiYikPVnyBdjjbECKVRFcldQH3ZtJzzA7FKVyJFaGvuQgoRZAMBZkeH6S8moYc0q4BvoqjhjRe6pmKN/bXQzqbglyvUO4L0oHB7rXPrL0RHs6ENtmZkUxcvTzTsAttru71YQ9R1u2a28beC/FiI48EBAAZipN6zuowWwbWpf4=', '1553403166'), ('3', 'RUS2UxldfDs7VsURmRAF9xVL0BTptl069pas3cSep+wzV5Xp8zh7cEP34eAjNMjDHPHfK2MJMpQmC/LhJzizzrQ+gTv09bOP+GHnqTZ17L6YO+KM4htRnNyTJweIak8CF0sLV21a8q2rqclk9WygJbHkWnLx6fFC/k24L2PXLR5LeP8dGrGICT32zkF4DdA53aHlEGEIvhXvqOWC06mBBdv2KuNqhG+iZTAK3hA5m3WGdzZL2VoBaVOjKhWwrDyROx9F8bPwEql1TYSIxkiX8WJZpZ2qy86aVUy/+VosX9PmFs9QtbQFQRXjFCCBgeL3NuJiYikPVnyBdjjbECKVRFcldQH3ZtJzzA7FKVyJFaGvuQgoRZAMBZkeH6S8moYc0q4BvoqjhjRe6pmKN/bXQzqbglyvUO4L0oHB7rXPrL0RHs6ENtmZkUxcvTzTsAttru71YQ9R1u2a28beC/FiI48EBAAZipN6zuowWwbWpf4=', '1553403188'), ('4', 'RUS2UxldfDs7VsURmRAF9xVL0BTptl069pas3cSep+wzV5Xp8zh7cEP34eAjNMjDHPHfK2MJMpQmC/LhJzizzrQ+gTv09bOP+GHnqTZ17L6YO+KM4htRnNyTJweIak8CF0sLV21a8q2rqclk9WygJbHkWnLx6fFC/k24L2PXLR5LeP8dGrGICT32zkF4DdA53aHlEGEIvhXvqOWC06mBBdv2KuNqhG+iZTAK3hA5m3WGdzZL2VoBaVOjKhWwrDyROx9F8bPwEql1TYSIxkiX8WJZpZ2qy86aVUy/+VosX9PmFs9QtbQFQRXjFCCBgeL3NuJiYikPVnyBdjjbECKVRFcldQH3ZtJzzA7FKVyJFaGvuQgoRZAMBZkeH6S8moYc0q4BvoqjhjRe6pmKN/bXQzqbglyvUO4L0oHB7rXPrL0RHs6ENtmZkUxcvTzTsAttru71YQ9R1u2a28beC/FiI48EBAAZipN6zuowWwbWpf4=', '1553403199'), ('5', 'RUS2UxldfDs7VsURmRAF9xVL0BTptl069pas3cSep+wzV5Xp8zh7cEP34eAjNMjDHPHfK2MJMpQmC/LhJzizzrQ+gTv09bOP+GHnqTZ17L6YO+KM4htRnNyTJweIak8CF0sLV21a8q2rqclk9WygJbHkWnLx6fFC/k24L2PXLR5LeP8dGrGICT32zkF4DdA53aHlEGEIvhXvqOWC06mBBdv2KuNqhG+iZTAK3hA5m3WGdzZL2VoBaVOjKhWwrDyROx9F8bPwEql1TYSIxkiX8WJZpZ2qy86aVUy/+VosX9PmFs9QtbQFQRXjFCCBgeL3NuJiYikPVnyBdjjbECKVRFcldQH3ZtJzzA7FKVyJFaGvuQgoRZAMBZkeH6S8moYc0q4BvoqjhjRe6pmKN/bXQzqbglyvUO4L0oHB7rXPrL0RHs6ENtmZkUxcvTzTsAttru71YQ9R1u2a28beC/FiI48EBAAZipN6zuowWwbWpf4=', '1553403211'), ('6', 'RUS2UxldfDs7VsURmRAF9xVL0BTptl069pas3cSep+wzV5Xp8zh7cEP34eAjNMjDHPHfK2MJMpQmC/LhJzizzrQ+gTv09bOP+GHnqTZ17L6YO+KM4htRnNyTJweIak8CF0sLV21a8q2rqclk9WygJbHkWnLx6fFC/k24L2PXLR5LeP8dGrGICT32zkF4DdA53aHlEGEIvhXvqOWC06mBBdv2KuNqhG+iZTAK3hA5m3WGdzZL2VoBaVOjKhWwrDyROx9F8bPwEql1TYSIxkiX8WJZpZ2qy86aVUy/+VosX9PmFs9QtbQFQRXjFCCBgeL3NuJiYikPVnyBdjjbECKVRFcldQH3ZtJzzA7FKVyJFaGvuQgoRZAMBZkeH6S8moYc0q4BvoqjhjRe6pmKN/bXQzqbglyvUO4L0oHB7rXPrL0RHs6ENtmZkUxcvTzTsAttru71YQ9R1u2a28beC/FiI48EBAAZipN6zuowWwbWpf4=', '1553403305'), ('7', 'RUS2UxldfDs7VsURmRAF9xVL0BTptl069pas3cSep+wzV5Xp8zh7cEP34eAjNMjDHPHfK2MJMpQmC/LhJzizzrQ+gTv09bOP+GHnqTZ17L6YO+KM4htRnNyTJweIak8CF0sLV21a8q2rqclk9WygJbHkWnLx6fFC/k24L2PXLR5LeP8dGrGICT32zkF4DdA53aHlEGEIvhXvqOWC06mBBdv2KuNqhG+iZTAK3hA5m3WGdzZL2VoBaVOjKhWwrDyROx9F8bPwEql1TYSIxkiX8WJZpZ2qy86aVUy/+VosX9PmFs9QtbQFQRXjFCCBgeL3NuJiYikPVnyBdjjbECKVRFcldQH3ZtJzzA7FKVyJFaGvuQgoRZAMBZkeH6S8moYc0q4BvoqjhjRe6pmKN/bXQzqbglyvUO4L0oHB7rXPrL0RHs6ENtmZkUxcvTzTsAttru71YQ9R1u2a28beC/FiI48EBAAZipN6zuowWwbWpf4=', '1553419340'), ('8', '7WCO6Qugn/RlNFFx73RZ0tbdGKUXUwyp3x7jES4oadSAwA0Y1a8WX/ZFALvqXGQLJeWASN6B7mPktf/R8WLICC5A9p547nODf7+GBBDTUtdQxG+VzJ6upGJYET7EQ2Kvll9vqsr2aFn1/ozsoUEmou1ldoQEv02MDyaZOjBJ6J+E6HAAtxCGvlcxj2GIVpsdybFQ+L9xa3iYOiCPqS/LnQ==', '1553419366'), ('9', '7WCO6Qugn/RlNFFx73RZ0tbdGKUXUwyp3x7jES4oadSAwA0Y1a8WX/ZFALvqXGQLJeWASN6B7mPktf/R8WLICC5A9p547nODf7+GBBDTUtdQxG+VzJ6upGJYET7EQ2Kvll9vqsr2aFn1/ozsoUEmou1ldoQEv02MDyaZOjBJ6J+E6HAAtxCGvlcxj2GIVpsdybFQ+L9xa3iYOiCPqS/LnQ==', '1553419375'), ('10', '7WCO6Qugn/RlNFFx73RZ0tbdGKUXUwyp3x7jES4oadSAwA0Y1a8WX/ZFALvqXGQLJeWASN6B7mPktf/R8WLICC5A9p547nODf7+GBBDTUtdQxG+VzJ6upGJYET7EQ2Kvll9vqsr2aFn1/ozsoUEmou1ldoQEv02MDyaZOjBJ6J+E6HAAtxCGvlcxj2GIVpsdybFQ+L9xa3iYOiCPqS/LnQ==', '1553419424'), ('11', '7WCO6Qugn/RlNFFx73RZ0g+Oyu+e2sslihY81fKmEnRp5K89W8ZZdC9OscA3dZbqY51wLG8M4ax99HAYyOG9r7BdqwgdKdCfocm8Q2JIX9P8VKpf7oKN1Mm26xFL9/glm2QgRmVg9UxBfqSVWQbBy63++pI96KDWD/5YPSZQs2Ew414KTzb8aVcZXxlqNOEx6hkH6VeuroYixmkXuD6/YA==', '1553419788'), ('12', 'PCYiccwRPBoiCtY9Dqc4E161wC/4uMVfGChg+m1htaTMMd7pwCF6AbW1L2qXtKrqj1ysO4U7oXiLkQNUl8y58BpH90TyvrBRnLvL1zUrtXdj9ZudhVUOMr65pqpeUK025wiuhEIh1CBhE25EC9htdHIT2UU47T/HlQvKb5wqeB8MbPVK7a7QgQsVUdudz2i46FYTJPMeI1m29UCW/Ce6hhQCMbuuVjdIYDp4w3CayCg=', '1553420044'), ('13', 'PCYiccwRPBoiCtY9Dqc4E161wC/4uMVfGChg+m1htaTMMd7pwCF6AbW1L2qXtKrqj1ysO4U7oXiLkQNUl8y58BpH90TyvrBRnLvL1zUrtXdj9ZudhVUOMr65pqpeUK025wiuhEIh1CBhE25EC9htdHIT2UU47T/HlQvKb5wqeB8MbPVK7a7QgQsVUdudz2i46FYTJPMeI1m29UCW/Ce6hhQCMbuuVjdIYDp4w3CayCg=', '1553420092'), ('14', 'PCYiccwRPBoiCtY9Dqc4E161wC/4uMVfGChg+m1htaTMMd7pwCF6AbW1L2qXtKrqj1ysO4U7oXiLkQNUl8y58BpH90TyvrBRnLvL1zUrtXdj9ZudhVUOMr65pqpeUK025wiuhEIh1CBhE25EC9htdHIT2UU47T/HlQvKb5wqeB8MbPVK7a7QgQsVUdudz2i46FYTJPMeI1m29UCW/Ce6hhQCMbuuVjdIYDp4w3CayCg=', '1553420112'), ('15', 'PCYiccwRPBoiCtY9Dqc4E161wC/4uMVfGChg+m1htaTMMd7pwCF6AbW1L2qXtKrqj1ysO4U7oXiLkQNUl8y58BpH90TyvrBRnLvL1zUrtXdj9ZudhVUOMr65pqpeUK025wiuhEIh1CBhE25EC9htdHIT2UU47T/HlQvKb5wqeB8MbPVK7a7QgQsVUdudz2i46FYTJPMeI1m29UCW/Ce6hhQCMbuuVjdIYDp4w3CayCg=', '1553420123'), ('16', 'PCYiccwRPBoiCtY9Dqc4E161wC/4uMVfGChg+m1htaTMMd7pwCF6AbW1L2qXtKrqj1ysO4U7oXiLkQNUl8y58BpH90TyvrBRnLvL1zUrtXdj9ZudhVUOMr65pqpeUK025wiuhEIh1CBhE25EC9htdHIT2UU47T/HlQvKb5wqeB8MbPVK7a7QgQsVUdudz2i46FYTJPMeI1m29UCW/Ce6hhQCMbuuVjdIYDp4w3CayCg=', '1553420154'), ('17', 'Bj8R71xGw9z8LhIyy5hl/z7dq3QaHfBibfBobX2SMNimxkCRU97i6YTI15GNWhHK+j4Cgnk4nk0uDfV871/caFN27qYX14l4fn7TQJDwEgIuQPaeeO5zg3+/hgQQ01LXUMRvlcyerqRiWBE+xENir5Zfb6rK9mhZ9f6M7KFBJqLtZXaEBL9NjA8mmTowSeifhOhwALcQhr5XMY9hiFabHcmxUPi/cWt4mDogj6kvy50=', '1553420464'), ('18', 'Bj8R71xGw9z8LhIyy5hl/z7dq3QaHfBibfBobX2SMNimxkCRU97i6YTI15GNWhHK+j4Cgnk4nk0uDfV871/caFN27qYX14l4fn7TQJDwEgIuQPaeeO5zg3+/hgQQ01LXUMRvlcyerqRiWBE+xENir5Zfb6rK9mhZ9f6M7KFBJqLtZXaEBL9NjA8mmTowSeifhOhwALcQhr5XMY9hiFabHcmxUPi/cWt4mDogj6kvy50=', '1553420490'), ('19', 'Bj8R71xGw9z8LhIyy5hl/z7dq3QaHfBibfBobX2SMNimxkCRU97i6YTI15GNWhHK+j4Cgnk4nk0uDfV871/caFN27qYX14l4fn7TQJDwEgIuQPaeeO5zg3+/hgQQ01LXUMRvlcyerqRiWBE+xENir5Zfb6rK9mhZ9f6M7KFBJqLtZXaEBL9NjA8mmTowSeifhOhwALcQhr5XMY9hiFabHcmxUPi/cWt4mDogj6kvy50=', '1553420566'), ('20', 'PCYiccwRPBoiCtY9Dqc4E161wC/4uMVfGChg+m1htaTMMd7pwCF6AbW1L2qXtKrqj1ysO4U7oXiLkQNUl8y58BpH90TyvrBRnLvL1zUrtXdj9ZudhVUOMr65pqpeUK025wiuhEIh1CBhE25EC9htdHIT2UU47T/HlQvKb5wqeB8MbPVK7a7QgQsVUdudz2i46FYTJPMeI1m29UCW/Ce6hhQCMbuuVjdIYDp4w3CayCg=', '1553437251'), ('21', 'Bj8R71xGw9z8LhIyy5hl/z7dq3QaHfBibfBobX2SMNimxkCRU97i6YTI15GNWhHK+j4Cgnk4nk0uDfV871/caFN27qYX14l4fn7TQJDwEgIuQPaeeO5zg3+/hgQQ01LXUMRvlcyerqRiWBE+xENir5Zfb6rK9mhZ9f6M7KFBJqLtZXaEBL9NjA8mmTowSeifhOhwALcQhr5XMY9hiFabHcmxUPi/cWt4mDogj6kvy50=', '1553442816'), ('22', 'Bj8R71xGw9z8LhIyy5hl/z7dq3QaHfBibfBobX2SMNi6KJgpL+uur1r1WVkWp2Mt5DNAjbRnJM6NXz7w0JhnhFN27qYX14l4fn7TQJDwEgIuQPaeeO5zg3+/hgQQ01LXUMRvlcyerqRiWBE+xENir5Zfb6rK9mhZ9f6M7KFBJqLtZXaEBL9NjA8mmTowSeifhOhwALcQhr5XMY9hiFabHelzyoiqPz77/r25Rxm6Vq8=', '1553442841'), ('23', '', '1553523977'), ('24', '', '1553523981'), ('25', '', '1553528293'), ('26', '', '1553528304'), ('27', '', '1553528319'), ('28', '', '1553528402'), ('29', '', '1553528407'), ('30', '', '1553528410'), ('31', '', '1553528455'), ('32', '', '1553528515'), ('33', '', '1553528748'), ('34', '', '1553528802'), ('35', '', '1553528866'), ('36', 'Bj8R71xGw9z8LhIyy5hl/z7dq3QaHfBibfBobX2SMNi6KJgpL+uur1r1WVkWp2Mt5DNAjbRnJM6NXz7w0JhnhFN27qYX14l4fn7TQJDwEgIuQPaeeO5zg3+/hgQQ01LXUMRvlcyerqRiWBE+xENir5Zfb6rK9mhZ9f6M7KFBJqLtZXaEBL9NjA8mmTowSeifhOhwALcQhr5XMY9hiFabHelzyoiqPz77/r25Rxm6Vq8=', '1553528882');
COMMIT;

-- ----------------------------
--  Table structure for `td_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `td_auth_group`;
CREATE TABLE `td_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT '1',
  `title` char(100) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_auth_group`
-- ----------------------------
BEGIN;
INSERT INTO `td_auth_group` VALUES ('1', '1', '后台管理员', '1', '0,4,5,6,7,8,1,3,2,');
COMMIT;

-- ----------------------------
--  Table structure for `td_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `td_auth_group_access`;
CREATE TABLE `td_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_auth_group_access`
-- ----------------------------
BEGIN;
INSERT INTO `td_auth_group_access` VALUES ('1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `td_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `td_auth_rule`;
CREATE TABLE `td_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pid` mediumint(8) DEFAULT '0',
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `usertype` int(1) DEFAULT '1',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `css` varchar(20) DEFAULT NULL,
  `show` char(1) DEFAULT '1' COMMENT '1展示 0隐藏',
  `sort` int(11) DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT '1',
  `condition` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_auth_rule`
-- ----------------------------
BEGIN;
INSERT INTO `td_auth_rule` VALUES ('1', '0', '/chaochao/Permission', '权限管理', '1', '0', 'fa fa-lock', '1', '10', '1', ''), ('2', '1', '/chaochao/Permission/rules', '权限列表', '1', '0', '', '1', '2', '1', ''), ('3', '1', '/chaochao/Permission/groups', '用户组列表', '1', '0', '', '1', '1', '1', ''), ('4', '0', '/chaochao/update/', '自我更新管理', '1', '0', 'fa fa-download', '1', '90', '1', ''), ('5', '0', '/chaochao/config', '配置信息', '1', '0', 'fa fa-file', '1', '80', '1', ''), ('6', '0', '/chaochao/bsprocess', '浏览器父进程', '1', '0', 'fa fa-compass', '1', '70', '1', ''), ('7', '0', '/chaochao/tackledlls', '拦截DLL', '1', '0', 'fa fa-hand-stop-o', '1', '60', '1', ''), ('8', '0', '/chaochao/tool', '工具', '1', '0', 'fa fa-wrench', '1', '50', '1', ''), ('9', '0', '/chaochao/log', '日志查看', '1', '0', 'fa fa-list', '1', '100', '1', ''), ('10', '8', '/chaochao/tool/aesTest', '加密测试工具', '1', '0', '', '1', '0', '1', ''), ('11', '9', '/chaochao/log/hostinfo', '开机上报日志', '1', '0', '', '1', '0', '1', ''), ('12', '4', '/chaochao/update/file', '自我更新配置', '1', '0', '', '1', '0', '1', ''), ('13', '5', '/chaochao/config/index', '配置信息列表', '1', '0', '', '1', '0', '1', ''), ('14', '6', '/chaochao/bsprocess/index', '父进程配置', '1', '0', '', '1', '0', '1', ''), ('15', '7', '/chaochao/tackledlls/index', '拦截DLL配置', '1', '0', '', '1', '0', '1', ''), ('16', '9', '/chaochao/log/update', '自我更新日志', '1', '0', '', '1', '0', '1', ''), ('17', '9', '/chaochao/log/getconfig', '获取配置日志', '1', '0', '', '1', '0', '1', ''), ('18', '9', '/chaochao/log/bsprocess', '浏览器父进程日志', '1', '0', '', '1', '0', '1', ''), ('19', '9', '/chaochao/log/tackledlls', '拦截DLL日志', '1', '0', '', '1', '0', '1', '');
COMMIT;

-- ----------------------------
--  Table structure for `td_auth_user`
-- ----------------------------
DROP TABLE IF EXISTS `td_auth_user`;
CREATE TABLE `td_auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `usertype` int(11) DEFAULT NULL,
  `nickname` varchar(20) DEFAULT NULL COMMENT '名称',
  `username` varchar(20) DEFAULT NULL COMMENT '账号',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `token` varchar(100) DEFAULT NULL COMMENT '接口验证token',
  `tokenexpire` int(11) DEFAULT NULL COMMENT 'token过期时间',
  `province` varchar(20) DEFAULT NULL,
  `city` varchar(20) DEFAULT NULL,
  `district` varchar(20) DEFAULT NULL,
  `frozen` tinyint(1) DEFAULT '0' COMMENT '是否冻结 0未冻结 1冻结',
  `avatar` varchar(200) DEFAULT NULL COMMENT '头像',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `createip` varchar(20) DEFAULT NULL COMMENT '注册IP',
  `lastlogintime` int(11) DEFAULT NULL COMMENT '最近登录时间',
  `lastloginip` varchar(20) DEFAULT NULL COMMENT '最近登录IP',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_auth_user`
-- ----------------------------
BEGIN;
INSERT INTO `td_auth_user` VALUES ('1', '0', '1', '管理员', 'admin', '1101', 'b7b251166d36c32d34676c80dbc64a0a', '62c3b34890a2ed7e0d5fc9143a257de5', null, null, null, null, '0', '/uploads/20190323/4f3eaea35ffb1c082ee64b86f828d5db.png', null, null, '1553329516', '127.0.0.1');
COMMIT;

-- ----------------------------
--  Table structure for `td_bsprocess`
-- ----------------------------
DROP TABLE IF EXISTS `td_bsprocess`;
CREATE TABLE `td_bsprocess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `process` text,
  `status` tinyint(1) DEFAULT '1' COMMENT '1可用 0禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_bsprocess`
-- ----------------------------
BEGIN;
INSERT INTO `td_bsprocess` VALUES ('1', '[\"fffsa\",\"fsafafa\",\"aaaa\"]', '0'), ('2', '[\"fffsa111\",\"fsafafa2222\",\"aaaa\"]', '1');
COMMIT;

-- ----------------------------
--  Table structure for `td_config`
-- ----------------------------
DROP TABLE IF EXISTS `td_config`;
CREATE TABLE `td_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shp` text COMMENT 'SETHOMEPAGE',
  `dtsc` text COMMENT 'DESKTOPSHORTCUT',
  `ief` text COMMENT 'IEFAVORITES',
  `status` tinyint(1) DEFAULT '1' COMMENT '1可用 0禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_config`
-- ----------------------------
BEGIN;
INSERT INTO `td_config` VALUES ('6', '{\"www.baidu.com\":[{\"360\\u5b89\\u5168\\u6d4f\\u89c8\\u5668\":\"*\\\\\\\\360chrome.exe\",\"CHANGEPARENT\":\"false\"}],\"www.jd.com\":[{\"360\\u5b89\\u5168\\u6d4f\\u89c8\\u5668\":\"*\\\\\\\\360chrome.exe\",\"CHANGEPARENT\":\"false\"},{\"Internet Explorer\":\"*\\\\\\\\iexplore.exe\",\"CHANGEPARENT\":\"false\"}]}', '{\"fqfq\":\"wwwww\",\"rrwqrq\":\"rwqrq1111\"}', '{\"gggg1\":\"1111111\"}', '1');
COMMIT;

-- ----------------------------
--  Table structure for `td_get_bsprocess_log`
-- ----------------------------
DROP TABLE IF EXISTS `td_get_bsprocess_log`;
CREATE TABLE `td_get_bsprocess_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curver` varchar(50) DEFAULT NULL,
  `hard_ware_id` char(16) DEFAULT NULL,
  `group` varchar(10) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_get_bsprocess_log`
-- ----------------------------
BEGIN;
INSERT INTO `td_get_bsprocess_log` VALUES ('2', '16.10.7.1', '00E04C4B2950', '1', '1553437251');
COMMIT;

-- ----------------------------
--  Table structure for `td_get_config_log`
-- ----------------------------
DROP TABLE IF EXISTS `td_get_config_log`;
CREATE TABLE `td_get_config_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curver` varchar(50) DEFAULT NULL,
  `hard_ware_id` char(16) DEFAULT NULL,
  `group` varchar(10) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `td_hostinfo`
-- ----------------------------
DROP TABLE IF EXISTS `td_hostinfo`;
CREATE TABLE `td_hostinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hard_ware_id` char(16) DEFAULT NULL,
  `outlets_num` varchar(100) DEFAULT NULL,
  `ins_time` datetime DEFAULT NULL,
  `drv_vsion` varchar(50) DEFAULT NULL,
  `system_vsion` varchar(50) DEFAULT NULL,
  `antivirus` varchar(50) DEFAULT NULL,
  `cur_time` datetime DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_hostinfo`
-- ----------------------------
BEGIN;
INSERT INTO `td_hostinfo` VALUES ('2', '00E04C4B2950', 'xiazaizhe', '2016-09-26 10:37:07', '16.9.8.1', 'Windows 7  32位', '未发现杀毒软件', '2016-10-06 09:36:46', '1553403211'), ('3', '00E04C4B2950', 'xiazaizhe', '2016-09-26 10:37:07', '16.9.8.1', 'Windows 7  32位', '未发现杀毒软件', '2016-10-06 09:36:46', '1553403305');
COMMIT;

-- ----------------------------
--  Table structure for `td_tackledlls`
-- ----------------------------
DROP TABLE IF EXISTS `td_tackledlls`;
CREATE TABLE `td_tackledlls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dll` text,
  `process` text,
  `status` tinyint(1) DEFAULT '1' COMMENT '1可用 0禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_tackledlls`
-- ----------------------------
BEGIN;
INSERT INTO `td_tackledlls` VALUES ('1', '[\"123.dll\",\"456.dll\",\"789.dll\"]', '{\"aaa\":[0,1],\"cccc\":[2]}', '1');
COMMIT;

-- ----------------------------
--  Table structure for `td_tackledlls_log`
-- ----------------------------
DROP TABLE IF EXISTS `td_tackledlls_log`;
CREATE TABLE `td_tackledlls_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curver` varchar(50) DEFAULT NULL,
  `hard_ware_id` char(16) DEFAULT NULL,
  `group` varchar(10) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_tackledlls_log`
-- ----------------------------
BEGIN;
INSERT INTO `td_tackledlls_log` VALUES ('2', '16.10.7.1', '00E04C4B2950', '1', '1553442841'), ('3', '16.10.7.1', '00E04C4B2950', '1', '1553528882');
COMMIT;

-- ----------------------------
--  Table structure for `td_update_file`
-- ----------------------------
DROP TABLE IF EXISTS `td_update_file`;
CREATE TABLE `td_update_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(50) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `port` varchar(10) DEFAULT NULL,
  `version` varchar(50) DEFAULT NULL,
  `md5` char(32) DEFAULT NULL,
  `key` char(32) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1' COMMENT '1可用 0禁用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_update_file`
-- ----------------------------
BEGIN;
INSERT INTO `td_update_file` VALUES ('1', '11122s', '222', '333', '4444', '55555', '66666', '1');
COMMIT;

-- ----------------------------
--  Table structure for `td_update_log`
-- ----------------------------
DROP TABLE IF EXISTS `td_update_log`;
CREATE TABLE `td_update_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curver` varchar(50) DEFAULT NULL,
  `hard_ware_id` char(16) DEFAULT NULL,
  `group` varchar(20) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `td_update_log`
-- ----------------------------
BEGIN;
INSERT INTO `td_update_log` VALUES ('1', '16.10.7.1', '00E04C4B2950', '1', '1553419366'), ('3', '16.10.7.1', '00E04C4B2950', '1', '1553419424');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
