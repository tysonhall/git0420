<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 应用设置
// +----------------------------------------------------------------------

return [
    // 应用名称
    'app_name'               => '',
    // 应用地址
    'app_host'               => '',
    // 应用调试模式
    'app_debug'              => true,
    // 应用Trace
    'app_trace'              => false,
    // 是否支持多模块
    'app_multi_module'       => true,
    // 入口自动绑定模块
    'auto_bind_module'       => false,
    // 注册的根命名空间
    'root_namespace'         => [],
    // 默认输出类型
    'default_return_type'    => 'html',
    // 默认AJAX 数据返回格式,可选json xml ...
    'default_ajax_return'    => 'json',
    // 默认JSONP格式返回的处理方法
    'default_jsonp_handler'  => 'jsonpReturn',
    // 默认JSONP处理方法
    'var_jsonp_handler'      => 'callback',
    // 默认时区
    'default_timezone'       => 'Asia/Shanghai',
    // 是否开启多语言
    'lang_switch_on'         => false,
    // 默认全局过滤方法 用逗号分隔多个
    'default_filter'         => '',
    // 默认语言
    'default_lang'           => 'zh-cn',
    // 应用类库后缀
    'class_suffix'           => false,
    // 控制器类后缀
    'controller_suffix'      => false,

    // +----------------------------------------------------------------------
    // | 模块设置
    // +----------------------------------------------------------------------

    // 默认模块名
    'default_module'         => 'index',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
    // 默认验证器
    'default_validate'       => '',
    // 默认的空模块名
    'empty_module'           => '',
    // 默认的空控制器名
    'empty_controller'       => 'Error',
    // 操作方法前缀
    'use_action_prefix'      => false,
    // 操作方法后缀
    'action_suffix'          => '',
    // 自动搜索控制器
    'controller_auto_search' => false,

    // +----------------------------------------------------------------------
    // | URL设置
    // +----------------------------------------------------------------------

    // PATHINFO变量名 用于兼容模式
    'var_pathinfo'           => 's',
    // 兼容PATH_INFO获取
    'pathinfo_fetch'         => ['ORIG_PATH_INFO', 'REDIRECT_PATH_INFO', 'REDIRECT_URL'],
    // pathinfo分隔符
    'pathinfo_depr'          => '/',
    // HTTPS代理标识
    'https_agent_name'       => '',
    // IP代理获取标识
    'http_agent_ip'          => 'X-REAL-IP',
    // URL伪静态后缀
    'url_html_suffix'        => 'html',
    // URL普通方式参数 用于自动生成
    'url_common_param'       => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type'         => 0,
    // 是否开启路由延迟解析
    'url_lazy_route'         => false,
    // 是否强制使用路由
    'url_route_must'         => false,
    // 合并路由规则
    'route_rule_merge'       => false,
    // 路由是否完全匹配
    'route_complete_match'   => false,
    // 使用注解路由
    'route_annotation'       => false,
    // 域名根，如thinkphp.cn
    'url_domain_root'        => '',
    // 是否自动转换URL中的控制器和操作名
    'url_convert'            => true,
    // 默认的访问控制器层
    'url_controller_layer'   => 'controller',
    // 表单请求类型伪装变量
    'var_method'             => '_method',
    // 表单ajax伪装变量
    'var_ajax'               => '_ajax',
    // 表单pjax伪装变量
    'var_pjax'               => '_pjax',
    // 是否开启请求缓存 true自动缓存 支持设置请求缓存规则
    'request_cache'          => false,
    // 请求缓存有效期
    'request_cache_expire'   => null,
    // 全局请求缓存排除规则
    'request_cache_except'   => [],
    // 是否开启路由缓存
    'route_check_cache'      => false,
    // 路由缓存的Key自定义设置（闭包），默认为当前URL和请求类型的md5
    'route_check_cache_key'  => '',
    // 路由缓存类型及参数
    'route_cache_option'     => [],

    // 默认跳转页面对应的模板文件
    'dispatch_success_tmpl'  => Env::get('think_path') . 'tpl/dispatch_jump.tpl',
    'dispatch_error_tmpl'    => Env::get('think_path') . 'tpl/dispatch_jump.tpl',

    // 异常页面的模板文件
    'exception_tmpl'         => Env::get('think_path') . 'tpl/think_exception.tpl',

    // 错误显示信息,非调试模式有效
    'error_message'          => '页面错误！请稍后再试～',
    // 显示错误信息
    'show_error_msg'         => false,
    // 异常处理handle类 留空使用 \think\exception\Handle
    'exception_handle'       => '',

    // auth配置 20181128 秋水
    'auth'                   => [
        'auth_on'           => 1, // 权限开关
        'auth_type'         => 1, // 认证方式，1为实时认证；2为登录认证。
        'auth_group'        => 'auth_group', // 用户组数据不带前缀表名
        'auth_group_access' => 'auth_group_access', // 用户-用户组关系不带前缀表
        'auth_rule'         => 'auth_rule', // 权限规则不带前缀表
        'auth_user'         => 'member', // 用户信息不带前缀表
        'super_admin'       => 1,       //超级管理id
    ],

    // apilog 开关
    'apilog' => true,

    // 接口返回码配置
    'api_code' => [
        0 => ['code'=>0,'msg'=>'请求成功','desc'=>''],
        // 通用错误
        40001 => ['code'=>40001, 'msg'=>'登录失败', 'desc'=>''],
        40002 => ['code'=>40002, 'msg'=>'帐号或密码错误', 'desc'=>''],
        40003 => ['code'=>40003, 'msg'=>'验证码错误', 'desc'=>''],
        40004 => ['code'=>40004, 'msg'=>'验证码过期', 'desc'=>''],
        40005 => ['code'=>40005, 'msg'=>'原密码错误', 'desc'=>''],
        40006 => ['code'=>40006, 'msg'=>'密码修改失败', 'desc'=>''],
        40007 => ['code'=>40007, 'msg'=>'注册失败', 'desc'=>''],
        40008 => ['code'=>40008, 'msg'=>'发送短信失败', 'desc'=>''],
        40009 => ['code'=>40009, 'msg'=>'两次输入不一致', 'desc'=>''],
        40010 => ['code'=>40010, 'msg'=>'密码长度不能小于6位', 'desc'=>''],
        40011 => ['code'=>40011, 'msg'=>'该手机号已注册', 'desc'=>''],
        40012 => ['code'=>40012, 'msg'=>'手机号输入错误', 'desc'=>''],
        40013 => ['code'=>40013, 'msg'=>'密码输入错误', 'desc'=>''],
        40014 => ['code'=>40014, 'msg'=>'登录失效，请重新登录', 'desc'=>''],
        // 参数错误
        40101 => ['code'=>40101, 'msg'=>'参数不能为空', 'desc'=>''],
        40102 => ['code'=>40102, 'msg'=>'不支持的操作类型', 'desc'=>''],
        40103 => ['code'=>40103, 'msg'=>'参数格式错误', 'desc'=>''],
        40104 => ['code'=>40104, 'msg'=>'请填写必填项', 'desc'=>''],
        // 数据错误
        40201 => ['code'=>40201, 'msg'=>'数据不存在', 'desc'=>''],
        40202 => ['code'=>40202, 'msg'=>'数据重复', 'desc'=>''],
        40203 => ['code'=>40203, 'msg'=>'当前状态无法操作', 'desc'=>''],
        // 权限问题
        41001 => ['code'=>41001, 'msg'=>'token验证失败', 'desc'=>''],
        41002 => ['code'=>41002, 'msg'=>'授权错误', 'desc'=>''],
        41003 => ['code'=>41003, 'msg'=>'用户被冻结', 'desc'=>''],
        41004 => ['code'=>41004, 'msg'=>'没有权限', 'desc'=>''],
        // 余额积分问题
        42000 => ['code'=>42000, 'msg'=>'', 'desc'=>''],
        42001 => ['code'=>42001, 'msg'=>'余额不足', 'desc'=>''],
        42002 => ['code'=>42002, 'msg'=>'剩余次数不足或已过有效期，请购买次数', 'desc'=>''],
        42003 => ['code'=>42003, 'msg'=>'报名人数已满', 'desc'=>''],
        42004 => ['code'=>42004, 'msg'=>'请勿重复报名', 'desc'=>''],
        // 充值、支付问题
        43000 => ['code'=>43000, 'msg'=>'', 'desc'=>''],
        43001 => ['code'=>43001, 'msg'=>'提现密码错误', 'desc'=>''],
        43002 => ['code'=>43002, 'msg'=>'发票金额错误', 'desc'=>''],
        43003 => ['code'=>43003, 'msg'=>'支付未成功，请核实', 'desc'=>''],
        43004 => ['code'=>43004, 'msg'=>'支付失败，您已选择其他支付方式', 'desc'=>''],
        43005 => ['code'=>43005, 'msg'=>'开票金额小于500元，无法开票', 'desc'=>''],
        // 操作类错误
        44001 => ['code'=>44001, 'msg'=>'保存失败', 'desc'=>''],
        44002 => ['code'=>44002, 'msg'=>'删除失败', 'desc'=>''],
        44003 => ['code'=>44003, 'msg'=>'批量删除失败', 'desc'=>''],
        44004 => ['code'=>44004, 'msg'=>'查询失败', 'desc'=>''],
        44005 => ['code'=>44005, 'msg'=>'请选择需要购买的场次', 'desc'=>''],
        44006 => ['code'=>44006, 'msg'=>'场券数量不够', 'desc'=>''],
        // OA错误
        50000 => ['code'=>50000, 'msg'=>'', 'desc'=>''],
        // 商城错误
        60000 => ['code'=>60000, 'msg'=>'', 'desc'=>''],
        60001 => ['code'=>60001, 'msg'=>'支付失败', 'desc'=>''],
        //
        70000 => ['code'=>70000, 'msg'=>'', 'desc'=>''],
        // 第三方错误
        80000 => ['code'=>80000, 'msg'=>'', 'desc'=>''],
        // 服务器错误
        90000 => ['code'=>90000, 'msg'=>'', 'desc'=>''],
    ],
];
