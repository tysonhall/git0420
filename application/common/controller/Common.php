<?php
/**
 * 秋水
 * 2018/11/28
 * 总公共控制类
 */
namespace app\common\controller;

use \think\Controller;
use \think\auth\Auth;

class Common extends Controller
{
	public $auth;
	/**
	 * 构造函数
	 * @author 秋水
	 * @DateTime 2018-11-28T21:54:24+0800
	 */
	public function __construct()
	{
		parent::__construct();

		$this->auth = Auth::instance();

		if(!$this->checkPermission()) {
			if(request()->isAjax()) {
				return json(['code'=>40001,'desc'=>'没有权限','msg'=>'访问失败']);
			} else {
				return $this->redirect('/tdmin/index/controlBoard');
			}
		}

		// 测试默认值
		$menu = $this->getMenuList(1, 1);
		$this->assign('menu', $menu);
		$this->assign('realname', '秋水');
	}

	/**
	 * 查看用户是否有当前操作的权限
	 * @author 秋水
	 * @DateTime 2018-11-29T17:22:51+0800
	 * @return   [type]                   [description]
	 */
	public function checkPermission()
	{
		$module = request()->module();
		$controller = request()->controller();
		$action = request()->action();
		// $url = request()->url();
		// $url = str_replace('.html', '', $url);
		$url = '/'.$module.'/'.$controller.'/'.$action;
		$url = strtolower($url);

		if(in_array($url, ['/tdmin/index/controlboard','/tdmin/index/index','/index/index/index'])) {
			return true;
		}
		return $this->checkAuth($url, 1);
	}

	/**
	 * 验证用户是否有权限
	 * @author 秋水
	 * @DateTime 2018-11-28T22:21:52+0800
	 * @param    int                   $rule
	 * @param    int                   $uid
	 * @return   bool
	 */
	public function checkAuth($rule, $uid)
	{
		if($this->auth->check($rule, $uid)){
		    return true;
		}else{
            return false;
		}
	}

	/**
	 * 获取菜单权限列表
	 * @author 秋水
	 * @DateTime 2018-11-29T15:39:30+0800
	 * @param    int                   $uid
	 */
	public function getMenuList($uid)
	{
		$ruleIdsArr = $this->getRuleIdsArr($uid);

		// 权限列表组合为有层级的数组
		$ruleList = \Db::name('AuthRule')->where([['id','in',$ruleIdsArr],['type','eq',1],['status','eq',1],['show','eq',1]])->order('sort desc')->select();
		$result = $this->ruleListToTree($ruleList, 0);
		return $result;
	}

	/**
	 * 获取用户的权限id
	 * @author 秋水
	 * @DateTime 2018-11-29T16:24:51+0800
	 */
	public function getRuleIdsArr($uid)
	{
		$groupAccess = \Db::name('AuthGroupAccess')->where([['uid','eq',$uid]])->select();
		// 用户绑定的角色id
		$ruleIds = '';
		foreach ($groupAccess as $key => $o) {
			$group = \Db::name('AuthGroup')->where([['id','eq',$o['group_id']]])->find();
			if($key > 0) {
				$ruleIds .= ',';
			}
			$ruleIds .= $group['rules'];
		}
		// 用户所有的权限id
		$ruleIdsArr = array_unique(explode(',', $ruleIds));
		return $ruleIdsArr;
	}

	/**
	 * 权限数组转换为树结构
	 * @author 秋水
	 * @DateTime 2018-11-29T15:48:30+0800
	 * @param    array                   $ruleList
	 * @param    int
	 */
	public function ruleListToTree($ruleList, $pid)
	{
		$r = [];
		foreach ($ruleList as $key => $o) {
			if($o['pid'] == $pid) {
				$item = $o;
				$item['children'] = $this->ruleListToTree($ruleList, $o['id']);
				$r[] = $item;
			}
		}
		return $r;
	}

	/**
	 * 获取表格分级格式的权限列表
	 * @author 秋水
	 * @DateTime 2018-11-29T16:51:00+0800
	 */
	public function ruleListToTable($ruleList, $pid, $level)
	{
		$r = [];
		$str = '';
		for ($i=0; $i < $level; $i++) { 
			$str .= '&nbsp;&nbsp;&nbsp;&nbsp;';
		}
		$level++;
		foreach ($ruleList as $key => $o) {
			if($o['pid'] == $pid) {
				$item = $o;
				$item['title'] = $str.$o['title'];
				$r[] = $item;
				$childrenR = $this->ruleListToTable($ruleList, $o['id'], $level);
				$r = array_merge($r, $childrenR);
			}
		}
		return $r;
	}

    /**
     * 获取表格分级格式的菜单列表
     * @author 江远
     * @DateTime 2018-12-05
     */
    public function categoryListToTable($categoryList, $pid, $level)
    {
        $r = [];
        $str = '';
        for ($i=0; $i < $level; $i++) {
            $str .= '—— ';
        }
        $level++;
        foreach ($categoryList as $key => $o) {
            if($o['pid'] == $pid) {
                $item = $o;
                $item['cat_name'] = $str.$o['cat_name'];
                $r[] = $item;
                $childrenR = $this->categoryListToTable($categoryList, $o['id'], $level);
                $r = array_merge($r, $childrenR);
            }
        }
        return $r;
    }
}
