<?php
namespace app\common\model;

use think\Model;

class Config extends Model
{
	public function index($data)
	{
		// $data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['GET']['CONFIG'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'curver' => $hostinfo['CURVER'],
			'group' => $hostinfo['GROUP'],
			'create_time' => time(),
		];

		$r = model('GetConfigLog')->insert($insertData);

		$config = $this->where([['status','eq',1]])->find();

		if(!$config) {
			$result = [
				"SETHOMEPAGE" => [],
				"DESKTOPSHORTCUT" => [],
				"IEFAVORITES" => [],
			];
		} else {
			$result = [
				"SETHOMEPAGE" => json_decode($config['shp'],true),
				"DESKTOPSHORTCUT" => json_decode($config['dtsc'],true),
				"IEFAVORITES" => json_decode($config['ief'],true),
			];
		}

		return $result;
		// $resultStr = $this->setData(json_encode($result));
		// echo $resultStr;
	}
}
