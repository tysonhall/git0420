<?php
namespace app\common\model;

use think\Model;

class TackledllsLog extends Model
{
	public function index($data)
	{
		// $data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['GET']['TACKLEDLLS'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'curver' => $hostinfo['CURVER'],
			'group' => $hostinfo['GROUP'],
			'create_time' => time(),
		];

		$r = $this->insert($insertData);

		$tackledlls = model('Tackledlls')->where([['status','eq',1]])->find();

		if(!$tackledlls) {
			$result = [
				"dll" => [],
				"process" => [],
			];
		} else {
			$result = [
				"dll" => json_decode($tackledlls['dll'],true),
				"process" => json_decode($tackledlls['process'],true),
			];
		}

		return $result;
		// $resultStr = $this->setData(json_encode($result));
		// echo $resultStr;
	}
}
