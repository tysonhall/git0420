<?php
namespace app\common\model;

use think\Model;

class Hostinfo extends Model
{
	public function index($data)
	{
		// $data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['PUT']['HOSTINFO'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'outlets_num' => $hostinfo['OutletsNum'],
			'ins_time' => $hostinfo['InsTime'],
			'drv_vsion' => $hostinfo['DrvVsion'],
			'system_vsion' => $hostinfo['SystemVsion'],
			'antivirus' => $hostinfo['Antivirus'],
			'cur_time' => $hostinfo['CurTime'],
			'create_time' => time(),
		];

		$r = $this->insert($insertData);

		return null;
	}
}
