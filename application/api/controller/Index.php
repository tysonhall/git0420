<?php
/**
 * 秋水
 * 默认
 */
namespace app\api\controller;

use think\Request;

class Index extends Common
{
	// public function index()
	// {
	// 	$path = request()->path();
	// 	$controller = $this->getController($path);

	// 	$data = $this->getData();
	// 	$r = model($controller)->index($data);
	// 	if($r) {
	// 		$resultStr = $this->setData(json_encode($r));
	// 		echo $resultStr;
	// 	}
	// }
	
	/**
	 * 转发
	 * @author 秋水
	 * @DateTime 2019-03-25T23:02:59+0800
	 */
	function __construct()
	{
		parent::__construct();
		$path = request()->path();
		$controller = $this->getController($path);
		$this->redirect('/api/'.$controller);
	}

	/**
	 * 获取接口名称
	 * @author 秋水
	 * @DateTime 2019-03-25T22:51:20+0800
	 */
	public function getController($path)
	{
		if(strlen($path) <= 0) {
			return 'index';
		}

		$index = 0;
		for ($i=0; $i < strlen($path); $i++) { 
			// ascii 码
			$ascii = ord($path[$i]);
			// 奇数
			if($i%2 == 0) {
				$index = $index + $ascii;
			} else {
				// 偶数
				$index = $index - $ascii;
			}
		}
		$controllerArr = [
			0 => 'put', // aa
			1 => 'update', // ba
			2 => 'getconfig', // ca
			3 => 'bsprocess', // da
			4 => 'tackledlls', // ea
			5 => ''
		];
		$controller = isset($controllerArr[$index])?$controllerArr[$index]:'index';
		return $controller;
	}
}