<?php
/**
 * 秋水
 * 接口公共控制类
 */
namespace app\api\controller;

use think\Controller;
use extend\Aes;
use think\Request;

class Common extends Controller
{

	var $key1Arr = [0xBB, 0x94, 0x79, 0xF1, 0xBB, 0x93, 0x78, 0xF1, 0xB9, 0x92, 0x77, 0xF1, 0xB8, 0x91, 0x77, 0xF0, 0xB8, 0x91, 0x77, 0xEF, 0xB7, 0x90, 0x75, 0xEF, 0xB6, 0x8E, 0x74, 0xEF, 0xB6, 0x8D, 0x73, 0xEF];
	var $key2Arr = [0xB5, 0x8C, 0x73, 0xEF, 0xB5, 0x8A, 0x71, 0xEF, 0xB4, 0x89, 0x71, 0xEF, 0xB3, 0x88, 0x6F, 0xEF, 0xB3, 0x88, 0x6F, 0xEF, 0xB2, 0x86, 0x6E, 0xEF, 0xB2, 0x85, 0x6D, 0xEF, 0xB1, 0x84, 0x6C, 0xEF];

	function __construct()
	{
		parent::__construct();

		// \app\api\controller\$controller;

		$apilog = config('apilog');
		if($apilog) {
			// 记录接口日志
			$insertData = [
				'content' => file_get_contents('php://input'),
				'create_time' => time()
			];

			$r = model('ApiLog')->insert($insertData);
		}
	}

	/**
	 * 获取接口名称
	 * @author 秋水
	 * @DateTime 2019-03-25T22:51:20+0800
	 */
	public function getController($path)
	{
		if(strlen($path) <= 0) {
			return 'index';
		}

		$index = 0;
		for ($i=0; $i < strlen($path); $i++) { 
			// ascii 码
			$ascii = ord($path[$i]);
			// 奇数
			if($i%2 == 0) {
				$index = $index + $ascii;
			} else {
				// 偶数
				$index = $index - $ascii;
			}
		}
		$controllerArr = [
			0 => 'hostinfo', // aa
			1 => 'updateLog', // ba
			2 => 'Config', // ca
			3 => 'bsprocessLog', // da
			4 => 'tackledllsLog', // ea
			5 => ''
		];
		$controller = isset($controllerArr[$index])?$controllerArr[$index]:'index';
		return $controller;
	}

	/**
	 * 获取解密后的参数
	 * @author 秋水
	 * @DateTime 2019-03-24T08:10:27+0800
	 */
	public function getData()
	{
		$Aes = new Aes();
		$key1 = $this->arrToStr($this->key1Arr);
		$key2 = $this->arrToStr($this->key2Arr);

		$str = file_get_contents('php://input');

		$strDecrypt = $Aes->decrypt($str, $key1, $key2);

		return $strDecrypt;
	}

	/**
	 * 获取加密后的参数
	 * @author 秋水
	 * @DateTime 2019-03-24T08:10:57+0800
	 */
	public function setData($str)
	{
		$Aes = new Aes();
		$key1 = $this->arrToStr($this->key1Arr);
		$key2 = $this->arrToStr($this->key2Arr);

		$strDecrypt = $Aes->encrypt($str, $key1, $key2);

		return $strDecrypt;
	}

	/**
	 * key数组转化为字符串
	 * @author 秋水
	 * @DateTime 2019-03-23T22:36:49+0800
	 */
	public function arrToStr($arr) {
		$str = '';
		foreach ($arr as $key => $o) {
			$str .= chr(hexdec($o));
		}
		return $str;
	}
}