<?php
/**
 * 秋水
 * 自我更新
 */
namespace app\api\controller;

use think\Request;

class Update extends Common
{
	/**
	 * 默认执行方法
	 * @author 秋水
	 * @DateTime 2019-03-24T08:12:43+0800
	 */
	public function index()
	{
		$data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['GET']['VERSION'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'curver' => $hostinfo['CURVER'],
			'group' => $hostinfo['GROUP'],
			'create_time' => time(),
		];

		$r = model('UpdateLog')->insert($insertData);

		$updateFile = model('UpdateFile')->where([['status','eq',1]])->find();

		if(!$updateFile) {
			$result = [
				"HOST" => '',
				"PATH" => '',
				"PORT" => '',
				"VERSION" => '',
				"MD5" => '',
				"KEY" => '',
			];
		} else {
			$result = [
				"HOST" => $updateFile['host'],
				"PATH" => $updateFile['path'],
				"PORT" => $updateFile['port'],
				"VERSION" => $updateFile['version'],
				"MD5" => $updateFile['md5'],
				"KEY" => $updateFile['key'],
			];
		}

		$resultStr = $this->setData(json_encode($result));
		echo $resultStr;
	}
}