<?php
/**
 * 秋水
 * 上报日志
 */
namespace app\api\controller;

use think\Request;

class Put extends Common
{
	/**
	 * 默认执行方法
	 * @author 秋水
	 * @DateTime 2019-03-24T08:12:43+0800
	 */
	public function index()
	{
		$data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['PUT']['HOSTINFO'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'outlets_num' => $hostinfo['OutletsNum'],
			'ins_time' => $hostinfo['InsTime'],
			'drv_vsion' => $hostinfo['DrvVsion'],
			'system_vsion' => $hostinfo['SystemVsion'],
			'antivirus' => $hostinfo['Antivirus'],
			'cur_time' => $hostinfo['CurTime'],
			'create_time' => time(),
		];

		$r = model('Hostinfo')->insert($insertData);
	}
}