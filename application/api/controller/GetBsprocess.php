<?php
/**
 * 秋水
 * 获取浏览器父进程
 */
namespace app\api\controller;

use think\Request;

class GetBsprocess extends Common
{
	/**
	 * 默认执行方法
	 * @author 秋水
	 * @DateTime 2019-03-24T08:12:43+0800
	 */
	public function index()
	{
		$data = $this->getData();
		$dataArr = json_decode($data, true);

		$hostinfo = $dataArr['GET']['BSRSTARTPROCESS'];

		$insertData = [
			'hard_ware_id' => $hostinfo['HardWareID'],
			'curver' => $hostinfo['CURVER'],
			'group' => $hostinfo['GROUP'],
			'create_time' => time(),
		];

		$r = model('GetBsprocessLog')->insert($insertData);

		$bsprocess = model('Bsprocess')->where([['status','eq',1]])->find();

		if(!$bsprocess) {
			$result = [
				"Process" => [],
			];
		} else {
			$result = [
				"Process" => json_decode($bsprocess['process'],true),
			];
		}

		$resultStr = $this->setData(json_encode($result));
		echo $resultStr;
	}
}