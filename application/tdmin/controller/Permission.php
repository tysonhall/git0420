<?php
/**
 * 秋水
 * 2018/11/28
 * 权限管理控制类
 */
namespace app\tdmin\controller;

use think\Db;
use clt\Leftnav;
use think\Controller;
use think\Request;

class Permission extends Common
{
    /**
     * 权限列表
     * @author 秋水
     * @DateTime 2018-11-29T16:03:27+0800
     */
    public function rules(Request $request)
    {
        if ($request->isAjax()) {
            $type = input('type', 0);
            $where = [['status', 'eq', 1]];
            if ($type != 0) {
                $where[] = ['type', 'eq', $type];
            }
            $ruleList = Db::name('AuthRule')->where($where)->order('sort desc')->select();
            $ruleTable = $this->ruleListToTable($ruleList, 0, 0);
            $result['data'] = $ruleTable;
            $result['count'] = Db::name('AuthRule')->where($where)->count();
            $result = array_merge($this->api_code[0], $result);
            return json($result);
        }
        return $this->fetch();
    }

    /***
     * 获取添加权限的权限列表
     * @author 江远
     */
    public function selectRules()
    {
        //获取权限列表
        $where = [];
        $where[] = ['pid', 'eq', 0];
        $ruleList = Db::name('AuthRule')->where($where)->order('sort desc')->field('id,title')->select();
        $data = [
            'id' => 0,
            'title' => '顶级权限',
        ];
        array_unshift($ruleList, $data);
        $result['data'] = $ruleList;
        $result['count'] = count($ruleList);
        $result = array_merge($this->api_code[0], $result);
        return json($result);
    }

    /***
     * 根据ID获取权限的信息
     */
    public function getRulesInfo()
    {
        $id = input('id');
        if (empty($id)) {
            $this->api_code[40001]['desc'] = '参数不正确！';
            return json($this->api_code[40001]);
        }
        $where[] = array('id', 'eq', $id);
        $info = Db::name('AuthRule')->where($where)->find();
        if ($info) {
            $this->api_code[0]['code'] = 0;
            $this->api_code[0]['msg'] = '获取成功！';
            $this->api_code[0]['data'] = $info;
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['desc'] = '获取失败！';
            return json($this->api_code[40001]);
        }
    }

    /***
     * 添加用户组权限
     * @author 江远
     * @DateTime 2018-11-29T18:05:15+0800
     */
    public function addRule(Request $request)
    {
        if ($request->isAjax()) {
            $pid = input('pid', 0);
            $name = input('name', '#');
            $title = input('title', '');
            $type = input('type', '');
            $css = input('css', '');
            $show = input('show', 1);
            $sort = input('sort', 0);
            $status = input('status', 1);
            $condition = input('condition', '');
            $insertData = [
                'pid' => $pid,
                'name' => $name,
                'title' => $title,
                'type' => $type,
                'css' => $css,
                'show' => $show,
                'sort' => $sort,
                'status' => $status,
                'condition' => $condition,
            ];
            $r = Db::name('AuthRule')->insert($insertData);
            if ($r) {
                $this->api_code[0]['msg'] = '添加权限成功';
                return json($this->api_code[0]);
            } else {
                $this->api_code[40001]['desc'] = '添加权限失败';
                return json($this->api_code[40001]);
            }
        }
        //获取权限列表
        $type = input('type', 1);
        $where = [['status', 'eq', 1]];
        if ($type != 0) {
            $where[] = ['type', 'eq', $type];
        }
        $ruleList = \Db::name('AuthRule')->where($where)->order('sort desc')->field('id,title')->select();
        $data = [
            'id' => 0,
            'title' => '顶级权限'
        ];
        array_unshift($ruleList, $data);
        $this->assign('ruleList', $ruleList);
        return $this->fetch();
    }

    /***
     * 编辑权限
     * @author 江远
     */
    public function editRule(Request $request)
    {
        if ($request->isAjax()) {
            $updateData = input();
            $r = model('AuthRule')->isUpdate(true)->allowField(true)->save($updateData);
            if ($r) {
                $this->api_code[0]['msg'] = '编辑权限成功！';
                return json($this->api_code[0]);
            } else {
                $this->api_code[40001]['desc'] = '编辑权限失败！';
                return json($this->api_code[40001]);
            }
        }
        $id = input('id') ? input('id') : 1;
        //获取权限列表
        $type = input('type', 1);
        $where = [['status', 'eq', 1]];
        if ($type != 0) {
            $where[] = ['type', 'eq', $type];
        }
        $ruleList = \Db::name('AuthRule')->where($where)->order('sort desc')->field('id,title')->select();
        $data = [
            'id' => 0,
            'title' => '顶级权限'
        ];
        array_unshift($ruleList, $data);
        $this->assign('ruleList', $ruleList);
        //获取默认的父级i
        $pid = \Db::name('AuthRule')->where('id', 'eq', $id)->value('pid');
        $this->assign('pid', $pid);
        $this->assign('id', $id);
        $info = db('auth_rule')->find($id);
        $this->assign('info', json_encode($info));
        return $this->fetch();
    }

    /***
     * 删除权限
     * @author 江远
     * desc 删除本级及下级权限
     */
    public function delRule()
    {
        //删除本级权限
        $id = input('id');
        $where[] = array('id', 'eq', $id);
        $r = Db::name('AuthRule')->where($where)->delete();
        //删除所包含的下级权限
        $wheres[] = array('pid', 'eq', $id);
        $r1 = Db::name('AuthRule')->where($wheres)->delete();
        if ($r || $r1) {
            $this->api_code[0]['msg'] = '删除权限成功！';
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['desc'] = '删除权限失败！';
            return json($this->api_code[40001]);
        }
    }

    /**
     * 用户组列表
     * @author 秋水
     * @DateTime 2018-11-29T16:05:06+0800
     */
    public function groups(Request $request)
    {
        if ($request->isAjax()) {
            $uid = input('uid', 0);
            $where[] = array('status', 'in', '1,0');
            if ($uid != 0) {
                $ruleIdsArr = $this->getRuleIdsArr($uid);
                $where[] = ['id', 'in', $ruleIdsArr];
            }
            $ruleList = Db::name('AuthGroup')->where($where)->order('id desc')->select();
            $result['data'] = $ruleList;
            $result['count'] = count($ruleList);
            $result = array_merge($this->api_code[0], $result);
            return json($result);
        }
        return $this->fetch();
    }

    /**
     * 用户组权限
     * @author 秋水
     * @DateTime 2018-11-29T16:05:15+0800
     */
    public function groupRules()
    {
        return $this->fetch();
    }

    /***
     * 添加用户组
     * @author 江远
     */
    public function addGroup(Request $request)
    {
        if ($request->isAjax()) {
            $title = input('title', '');
            $status = input('status', '1');
            $insertData = [
                'title' => $title,
                'status' => $status,
            ];
            $r = Db::name('AuthGroup')->insert($insertData);
            if ($r) {
                $this->api_code[0]['msg'] = '添加用户组成功！';
                return json($this->api_code[0]);
            } else {
                $this->api_code[40001]['msg'] = '添加用户组失败！';
                return json($this->api_code[40001]);
            }
        }
        return $this->fetch();
    }

    /***
     * 编辑用户组
     * @author 江远
     */
    public function editGroup(Request $request)
    {
        if ($request->isAjax()) {
            $updateData = input();
            $r = Db::name('AuthGroup')->where('id',$updateData['id'])->update(['title'=>$updateData['title'],'status'=>$updateData['status']]);
            if ($r) {
                $this->api_code[0]['msg'] = '编辑用户组成功！';
                return json($this->api_code[0]);
            } else {
                $this->api_code[40001]['msg'] = '编辑用户组失败！';
                return json($this->api_code[40001]);
            }
        }
        $id = input('id');
        if (empty($id)) {
            $this->error('参数不正确！');
        }
        $where[] = array('id', 'eq', $id);
        $info = \Db::name('AuthGroup')->where($where)->find();
        $this->assign('info', json_encode($info, true));
        $this->assign('id', $id);
        return $this->fetch();
    }

    /***
     * 获取用户组的信息
     */
    public function getGroupInfo()
    {
        $id = input('id');
        if (empty($id)) {
            $this->api_code[40001]['msg'] = '参数不正确！';
            return json($this->api_code[40001]);
        }
        $where[] = array('id', 'eq', $id);
        $info = Db::name('AuthGroup')->where($where)->find();
        if ($info) {
            $this->api_code[0]['code'] = 0;
            $this->api_code[0]['msg'] = '获取成功！';
            $this->api_code[0]['data'] = $info;
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['msg'] = '获取用户组失败！';
            return json($this->api_code[40001]);
        }
    }

    /***
     * 删除用户组
     * @author 江远
     */
    public function delGroup()
    {
        //删除本级权限
        $id = input('id');
        $where[] = array('id', 'eq', $id);
        $r = Db::name('AuthGroup')->where($where)->delete();
        if ($r) {
            $this->api_code[0]['msg'] = '删除权限成功！';
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['msg'] = '删除权限失败！';
            return json($this->api_code[40001]);
        }
    }

    /***
     * 添加用户组权限
     */
    public function groupAccess()
    {
        if(request()->isPost()) {
            $rules = input('post.rules');
            if(empty($rules)){
                return array('msg'=>'请选择权限!','code'=>0);
            }
            $data = input('post.');
            unset($data['token']);
            $where[] = array('id','eq',$data['id']);
            if(\Db::name('AuthGroup')->where($where)->update($data) !== false){
                $this->api_code[0]['msg'] = '权限配置成功！';
                $this->api_code[0]['url'] = url('groups');
                return json($this->api_code[0]);
            }else{
                $this->api_code[40001]['msg'] = '权限配置失败！';
                return json($this->api_code[40001]);
            }
        } else {
            $nav = new Leftnav();
            $admin_rule = db('auth_rule')->field('id,pid,title')->order('sort asc')->select();
            $rules = db('auth_group')->where('id', input('id'))->value('rules');
            $arr = $nav->auth($admin_rule, $pid = 0, $rules);
            $arr[] = array(
                "id" => 0,
                "pid" => 0,
                "title" => "全部",
                "open" => true
            );
            $this->assign('data', json_encode($arr, true));
            return $this->fetch();
        }
    }

    /***
     * 权限配置
     * @author 江远
     */
    public function groupSetAccess()
    {
        $rules = input('post.rules');
        if (empty($rules)) {
            return array('msg' => '请选择权限!', 'code' => 0);
        }
        $data = input('post.');
        unset($data['token']);
        $where[] = array('id', 'eq', $data['id']);
        if (\Db::name('AuthGroup')->where($where)->update($data) !== false) {
            $this->api_code[0]['msg'] = '权限配置成功！';
            $this->api_code[0]['url'] = url('/tdmin/permission/groups');
            return json($this->api_code[0]);
        } else {
            $this->api_code[40001]['msg'] = '权限配置失败！';
            return json($this->api_code[40001]);
        }
    }


}
