<?php
/**
 * 日志管理控制器
 */
namespace app\tdmin\controller;

class Log extends Common
{
	public function index()
	{

	}

	/**
	 * 开机上报日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:44:47+0800
	 */
	public function hostinfo()
	{
		return $this->fetch();
	}

	/**
	 * 开机上报日志列表获取
	 * @author 秋水
	 * @DateTime 2019-03-24T17:50:28+0800
	 */
	public function hostinfoList()
	{
		$page = input('page', 1);
		$limit = input('limit', 15);
		$where = [];
        $list = model('Hostinfo')->where($where)->page($page, $limit)->order('create_time desc')->select()->toArray();
        $count = model('Hostinfo')->where($where)->count();

        foreach ($list as $key => $o) {
        	$list[$key]['create_time'] = date('Y-m-d H:i:s', $o['create_time']);
        }

        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
	}

	/**
	 * 删除上报日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:55:10+0800
	 */
	public function delHostinfo()
	{
		$id = input('id', 0);
		$where = [['id','eq',$id]];

		$r = model('Hostinfo')->where($where)->delete();

		if($r) {
			return json(['code'=>0,'msg'=>'删除成功']);
		} else {
			return json(['code'=>40001,'msg'=>'删除失败']);
		}
	}

	/**
	 * 自我更新日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:44:47+0800
	 */
	public function update()
	{
		return $this->fetch();
	}

	/**
	 * 自我更新日志列表获取
	 * @author 秋水
	 * @DateTime 2019-03-24T17:50:28+0800
	 */
	public function updateList()
	{
		$page = input('page', 1);
		$limit = input('limit', 15);
		$where = [];
        $list = model('UpdateLog')->where($where)->page($page, $limit)->order('create_time desc')->select()->toArray();
        $count = model('UpdateLog')->where($where)->count();

        foreach ($list as $key => $o) {
        	$list[$key]['create_time'] = date('Y-m-d H:i:s', $o['create_time']);
        }

        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
	}

	/**
	 * 删除自我更新日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:55:10+0800
	 */
	public function delUpdate()
	{
		$id = input('id', 0);
		$where = [['id','eq',$id]];

		$r = model('UpdateLog')->where($where)->delete();

		if($r) {
			return json(['code'=>0,'msg'=>'删除成功']);
		} else {
			return json(['code'=>40001,'msg'=>'删除失败']);
		}
	}

	/**
	 * 拉取配置日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:44:47+0800
	 */
	public function getConfig()
	{
		return $this->fetch();
	}

	/**
	 * 拉取配置日志列表获取
	 * @author 秋水
	 * @DateTime 2019-03-24T17:50:28+0800
	 */
	public function getConfigList()
	{
		$page = input('page', 1);
		$limit = input('limit', 15);
		$where = [];
        $list = model('GetConfigLog')->where($where)->page($page, $limit)->order('create_time desc')->select()->toArray();
        $count = model('GetConfigLog')->where($where)->count();

        foreach ($list as $key => $o) {
        	$list[$key]['create_time'] = date('Y-m-d H:i:s', $o['create_time']);
        }

        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
	}

	/**
	 * 删除拉取配置日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:55:10+0800
	 */
	public function delGetConfig()
	{
		$id = input('id', 0);
		$where = [['id','eq',$id]];

		$r = model('GetConfigLog')->where($where)->delete();

		if($r) {
			return json(['code'=>0,'msg'=>'删除成功']);
		} else {
			return json(['code'=>40001,'msg'=>'删除失败']);
		}
	}

	/**
	 * 浏览器父进程日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:44:47+0800
	 */
	public function bsprocess()
	{
		return $this->fetch();
	}

	/**
	 * 浏览器父进程日志列表获取
	 * @author 秋水
	 * @DateTime 2019-03-24T17:50:28+0800
	 */
	public function bsprocessList()
	{
		$page = input('page', 1);
		$limit = input('limit', 15);
		$where = [];
        $list = model('GetBsprocessLog')->where($where)->page($page, $limit)->order('create_time desc')->select()->toArray();
        $count = model('GetBsprocessLog')->where($where)->count();

        foreach ($list as $key => $o) {
        	$list[$key]['create_time'] = date('Y-m-d H:i:s', $o['create_time']);
        }

        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
	}

	/**
	 * 删除浏览器父进程日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:55:10+0800
	 */
	public function delBsprocess()
	{
		$id = input('id', 0);
		$where = [['id','eq',$id]];

		$r = model('GetBsprocessLog')->where($where)->delete();

		if($r) {
			return json(['code'=>0,'msg'=>'删除成功']);
		} else {
			return json(['code'=>40001,'msg'=>'删除失败']);
		}
	}

	/**
	 * 拦截DLL日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:44:47+0800
	 */
	public function tackledlls()
	{
		return $this->fetch();
	}

	/**
	 * 拦截DLL日志列表获取
	 * @author 秋水
	 * @DateTime 2019-03-24T17:50:28+0800
	 */
	public function tackledllsList()
	{
		$page = input('page', 1);
		$limit = input('limit', 15);
		$where = [];
        $list = model('TackledllsLog')->where($where)->page($page, $limit)->order('create_time desc')->select()->toArray();
        $count = model('TackledllsLog')->where($where)->count();

        foreach ($list as $key => $o) {
        	$list[$key]['create_time'] = date('Y-m-d H:i:s', $o['create_time']);
        }

        return json(['code' => 0, 'msg' => '请求成功', 'count' => $count, 'data' => $list]);
	}

	/**
	 * 删除拦截DLL日志
	 * @author 秋水
	 * @DateTime 2019-03-24T17:55:10+0800
	 */
	public function delTackledlls()
	{
		$id = input('id', 0);
		$where = [['id','eq',$id]];

		$r = model('TackledllsLog')->where($where)->delete();

		if($r) {
			return json(['code'=>0,'msg'=>'删除成功']);
		} else {
			return json(['code'=>40001,'msg'=>'删除失败']);
		}
	}
}