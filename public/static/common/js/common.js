
function fill_option(list, data) {
    data=data||null;
    html = "";
    if (Array.isArray(list)) {
        for(var i=0;i<list.length;i++){
            if (list[i]) {
                id = list[i].id;
                title = list[i].title;
                if (typeof data == "undefined" || data == null || data == "") {
                    selected = "";
                } else {
                    selected = "selected";
                }
                html = html + "<option value="+id+" "+selected+">"+title+"</option>\r\n";
            }else{
                if (list[i] === data) {
                    selected = "selected";
                } else {
                    selected = "";
                }
                html = html + "<option value="+list[i]+" "+selected+">"+list[i]+"</option>\r\n";
            }
        }
    }
    return html;
}

function index_option(list){
    html = "";
    if (Array.isArray(list)) {
        for(var i=0;i<list.length;i++){
            for( j in list[i]){
                id = j;
                item = list[i][j];
                html = html + "<option value="+id+">"+item+"</option>\r\n";
            }
        }
    }
    return html;
}

